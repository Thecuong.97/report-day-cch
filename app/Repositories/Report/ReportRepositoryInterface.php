<?php

namespace App\Repositories\Report;

use App\Repositories\BaseRepositoryInterface;

interface ReportRepositoryInterface extends BaseRepositoryInterface
{
    public function getReportByReportId($reportId);
}
