<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    //Get the corresponding model
    public function getModel()
    {
        return \App\Models\User::class;
    }

    public function join()
    {
       return $this->model ->join('teams', 'users.team_id', '=', 'teams.id')
                            ->join('reports', 'users.id', 'reports.user_id')
                            ->select('users.*', 'reports.*', 'reports.id as id_report', 'teams.name as teamname')
                            ->orderByDesc('reports.id')
                            ->paginate(3);
    }

    public function joinUserWithId($id)
    {
        return $this->model->join('teams', 'users.team_id', '=', 'teams.id')
                            ->join('reports', 'users.id', 'reports.user_id')
                            ->select('users.*', 'reports.*', 'reports.id as id_report', 'teams.name as teamname')
                            ->where('users.id',$id)
                            ->orderByDesc('reports.id')
                            ->paginate(15);
    }

    public function showWithDate($date)
    {
        return $this->model->join('teams', 'users.team_id', '=', 'teams.id')
                            ->join('reports', 'users.id', 'reports.user_id')
                            ->select('users.*', 'reports.*', 'reports.id as id_report', 'teams.name as teamname')
                            ->orderByDesc('reports.report_day')
                            ->where('report_day', '=', $date)
                            ->get();
    }

    public function searchName($searchWord)
    {
        return $this->model->join('teams', 'users.team_id', '=', 'teams.id')
                    ->join('reports', 'users.id', 'reports.user_id')
                    ->select('users.*', 'reports.*', 'reports.id as id_report', 'teams.name as teamname')
                    ->orderByDesc('reports.report_day')
                    ->where('users.name', 'LIKE', '%' . $searchWord . '%')
                    ->get();
    }
}
