<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    public function join();
    public function joinUserWithId($id);
    public function showWithDate($date);
    public function searchName($searchWord);
}
