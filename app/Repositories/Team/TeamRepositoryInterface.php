<?php

namespace App\Repositories\Team;

use App\Repositories\BaseRepositoryInterface;

interface TeamRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Access the users of this team_id
     *
     * @param int $teamId
     */
    public function getUserByTeamId($teamId);
}
