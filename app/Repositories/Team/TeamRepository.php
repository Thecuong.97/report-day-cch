<?php

namespace App\Repositories\Team;

use App\Repositories\BaseRepository;

class TeamRepository extends BaseRepository implements TeamRepositoryInterface
{
    //Get the corresponding model
    public function getModel()
    {
        return \App\Models\Team::class;
    }

    /**
     * Access the users of this team_id
     *
     * @param int $teamId
     */
    public function getUserByTeamId($teamId)
    {
        $user = $this->model->find($teamId);
        return $user->users;
    }
}
