<?php

namespace App\Http\Controllers;

use App\Http\Requests\Team\StoreRequest;
use App\Http\Requests\Team\UpdateRequest;
use App\Models\User;
use App\Repositories\Team\TeamRepository;

class TeamController extends Controller
{
    protected $teamRepository;

    public function __construct(TeamRepository $teamRepository)
    {
        $this->teamRepository = $teamRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listTeam = $this->teamRepository->getAll();
        return response()->json($listTeam);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data = $request->all();
        $team = $this->teamRepository->create($data);
        return response()->json(['team' => $team, 'message' => 'Create succsess']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = $this->teamRepository->getById($id);
        $team->load('users', 'reports', 'comments');
        return response()->json($team);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $teamId
     * @return \Illuminate\Http\Response
     */
    public function showUserByTeam($teamId)
    {
        $userByTeam = $this->teamRepository->getUserByTeamId($teamId);
        return response()->json($userByTeam);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $data = $request->all();
        $team = $this->teamRepository->update($id, $data);
        return response()->json(['team' => $team, 'message' => 'Update succsess']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->teamRepository->delete($id);
        return response()->json(['message' => 'Delete succsess']);
    }
}
