<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Report\ReportRepository;


class ReportController extends Controller
{
    protected $reportReponsitory;

    public function __construct(ReportRepository $reportReponsitory)
    {
        $this->reportReponsitory = $reportReponsitory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listReport = $this->reportReponsitory->getAll();
        return response()->json($listReport);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $report = $this->reportReponsitory->create($data);
        return response()->json($report);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = $this->reportReponsitory->getById($id);
        $report->load('comments');
        return response()->json($report);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = $this->reportReponsitory->getById($id);
        return response()->json($report);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $report = $this->reportReponsitory->update($id, $data);
        return response()->json($report);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->reportReponsitory->delete($id);
        return response()->json(['message' => 'Delete succsess']);
    }
}
