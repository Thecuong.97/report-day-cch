<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'password' => Hash::make($this->get('password')),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'email' => 'required|email',
            'phone' => 'required|min:10|max:12',
            'password' => 'required|min:6',
            'address' => 'required|max:255',
            'birthday' => 'required|date|before:now',
            'gender' => 'required|boolean',
            'roles' => 'required|boolean',
            'team_id' => 'required',
            'avatar' => 'required'
        ];
    }
}
