<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'password' => Hash::make($this->get('password')),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|min:2|max:255',
            'email' => 'sometimes|email',
            'phone' => 'sometimes|min:10|max:12',
            'password' => 'required|min:6',
            'address' => 'sometimes|max:255',
            'birthday' => 'sometimes|date|before:now',
            'gender' => 'sometimes|boolean',
            'roles' => 'sometimes|boolean',
            'team_id' => 'sometimes',
            'avatar' => 'sometimes'
        ];
    }
}
