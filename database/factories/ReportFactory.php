<?php

namespace Database\Factories;

use App\Models\Report;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Report::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1,20),
            'team_id' => $this->faker->numberBetween(1,8),
            'work' => $this->faker->text(),
            'report_day' => $this->faker->dateTimeBetween('now'),
            'experience' => $this->faker->text(),
            'difficult' => $this->faker->text(),
            'suggest' => $this->faker->text(),
        ];
    }
}
