<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Team::factory(8)->create();
        DB::table('teams')->insert([
            'name' => 'Dev 1',
        ]);
        DB::table('teams')->insert([
            'name' => 'Dev 2',
        ]);
        DB::table('teams')->insert([
            'name' => 'Comter',
        ]);
    }
}
