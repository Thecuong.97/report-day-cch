<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('users')->insert([
            'name' => 'Trần Vĩ Đại',
            'email' => 'daivitran.dev@gmail.com',
            'phone' => '0325869635',
            'password' => Hash::make(123456),
            'address' => 'Kim Động - Hưng Yên',
            'birthday' => '2001-08-01',
            'gender' => 1,
            'roles' => 1,
            'avatar' => 'https://images.pexels.com/photos/1366630/pexels-photo-1366630.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
            'team_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'name' => 'Đoàn Thị Nụ',
            'email' => 'duong.doan200@gmail.com',
            'phone' => '0326076450',
            'password' => Hash::make(123456),
            'address' => 'Lương Tài - Bắc Ninh',
            'birthday' => '1992-12-10',
            'gender' => 1,
            'roles' => 0,
            'avatar' => 'https://images.pexels.com/photos/6533361/pexels-photo-6533361.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
            'team_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'name' => 'Nguyễn Thị Hồng Ngọc',
            'email' => 'nguyenngoc3112hlqn@gmail.com',
            'phone' => '0384152666',
            'password' => Hash::make(123456),
            'address' => 'Hạ Long - Quảng Ninh',
            'birthday' => '1998-12-31',
            'gender' => 0,
            'roles' => 0,
            'avatar' => 'https://images.pexels.com/photos/6248993/pexels-photo-6248993.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
            'team_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('users')->insert([
            'name' => 'Đinh Tiến Chương',
            'email' => 'chuongdt.dev@gmail.com',
            'phone' => '0348921234',
            'password' => Hash::make(123456),
            'address' => 'Lương Tài - Bắc Ninh',
            'birthday' => '1998-08-23',
            'gender' => 1,
            'roles' => 0,
            'avatar' => 'https://images.pexels.com/photos/1903611/pexels-photo-1903611.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
            'team_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
