<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Comment::factory(50)->create();
        DB::table('comments')->insert([
            'content' => Str::random(10),
            'user_id' => 1,
            'report_id' => 1,
            'team_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comments')->insert([
            'content' => Str::random(10),
            'user_id' => 2,
            'report_id' => 2,
            'team_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comments')->insert([
            'content' => Str::random(10),
            'user_id' => 3,
            'report_id' => 3,
            'team_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('comments')->insert([
            'content' => Str::random(10),
            'user_id' => 4,
            'report_id' => 3,
            'team_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
