<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Report::factory(30)->create();
        DB::table('reports')->insert([
            'work' => Str::random(10),
            'report_day' => Carbon::now(),
            'experience' => 'Không có gì',
            'difficult' => 'Bài dài',
            'suggest' => 'Cần thêm thời gian',
            'user_id' => 1,
            'team_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('reports')->insert([
            'work' => Str::random(10),
            'report_day' => Carbon::now(),
            'experience' => 'Không có gì',
            'difficult' => 'Bài dài',
            'suggest' => 'Cần thêm thời gian',
            'user_id' => 2,
            'team_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('reports')->insert([
            'work' => Str::random(10),
            'report_day' => Carbon::now(),
            'experience' => 'Không có gì',
            'difficult' => 'Bài dài',
            'suggest' => 'Cần thêm thời gian',
            'user_id' => 3,
            'team_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('reports')->insert([
            'work' => Str::random(10),
            'report_day' => Carbon::now(),
            'experience' => 'Không có gì',
            'difficult' => 'Bài dài',
            'suggest' => 'Cần thêm thời gian',
            'user_id' => 4,
            'team_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
