<?php

use App\Http\Controllers\TeamController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\CommentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('all', [UserController::class, 'join']);
Route::get('all/{id}', [UserController::class, 'joinUserWithId']);
Route::get('showWithDate/{date}', [UserController::class, 'showWithDate']);
Route::get('/like={search}', [UserController::class, 'searchName']);

// api table teams
Route::get('teams', [TeamController::class, 'index']);
Route::get('teams/{id}', [TeamController::class, 'show']);
Route::get('teams/users/{id}', [TeamController::class, 'showUserByTeam']);
Route::post('teams', [TeamController::class, 'store']);
Route::put('teams/{id}', [TeamController::class, 'update']);
Route::delete('teams/{id}', [TeamController::class, 'destroy']);

// api table users
Route::get('users', [UserController::class, 'index']);
Route::get('users/{id}', [UserController::class, 'show']);
Route::post('users', [UserController::class, 'store']);
Route::put('users/{id}', [UserController::class, 'update']);
Route::delete('users/{id}', [UserController::class, 'destroy']);

// api table reports
Route::get('reports', [ReportController::class, 'index']);
Route::get('reports/{id}', [ReportController::class, 'show']);
Route::post('reports', [ReportController::class, 'store']);
Route::put('reports/{id}', [ReportController::class, 'update']);
Route::delete('reports/{id}', [ReportController::class, 'destroy']);


// api table comment
Route::get('comments', [CommentController::class, 'index']);
Route::get('comments/{id}', [CommentController::class, 'show']);
Route::post('comments', [CommentController::class, 'store']);
Route::put('comments/{id}', [CommentController::class, 'update']);
Route::delete('comments/{id}', [CommentController::class, 'destroy']);



Route::group(['prefix' => 'auth'], function () {
    Route::get('test', [UserController::class, 'test']);
    Route::post('register', [UserController::class,'register']);
    Route::post('login', [UserController::class,'authenticate']);
});

Route::group(['middleware' => ['jwt.verify']], function () {
    //API User
    Route::group(['prefix' => 'auth'], function () {
        Route::get('user',[UserController::class, 'getAuthenticatedUser'] );
        Route::get('logout', [UserController::class, 'logout']);
    });
});
